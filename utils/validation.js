const { validationResult } = require('express-validator');
const crypto = require('crypto');

const User = require('../mongo/model/user');

function checkValidation(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
}

function passwordEncoding(password){
  return crypto.createHash('sha256').update(password, 'utf8').digest().toString('base64');
}

function updateLastseen(user) {
  if (user !== null || user !== undefined) {
      user.lastseen = new Date(Date.now()).toISOString();
      User.findOneAndUpdate({ _id: user._id }, user, function (err, doc) {
          if (err)
              console.log(err);
      });
  }
}

module.exports.checkValidation = checkValidation;
module.exports.passwordEncoding = passwordEncoding;
module.exports.updateLastseen = updateLastseen;