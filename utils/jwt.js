const jwt = require("jsonwebtoken");
const JWT_SECRET = "secret";

function hasValidJwt(req, res, next) {
    if (req.headers && req.headers["authorization"]) {
        if (req.headers["authorization"].indexOf("Bearer ") !== -1) {
            const accessToken = req.headers["authorization"].split("Bearer ")[1];
            return jwt.verify(accessToken, JWT_SECRET, function (err, decoded) {
                if (err) {
                    return res.status(401).json({
                        error: "Unauthorized",
                        message: "Invalid or expired token"
                    });
                }
                res.locals.decodedUser = decoded.user;
                next();
            });
        }
    }
    return res.status(401).json({
        error: "Unauthorized",
        message: "Missing or invalid Autentication Header"
    });
}

function generateJWT(user) {
    const accessToken = jwt.sign({ user }, JWT_SECRET, { expiresIn: "1 hour" });
    return {
        accessToken,
        expiresIn: 3600
    };
}

module.exports.generateJWT = generateJWT;
module.exports.jwtIsValid = hasValidJwt;