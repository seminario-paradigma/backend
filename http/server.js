const express = require('express');
const authRouter = require('./routes/auth')
const blogRouter = require('./routes/articles')
const usersRouter = require('./routes/users')
const cors = require('cors');

const app = express();
const port = process.env.PORT || '8080';

const server = app.listen(port)
    .on('error', onError)
    .on('listening', onListening);

app.get('/', function (req, res) {
    res.send("Hello World!");
})

app.use(cors({ origin: '*' }))
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use(express.urlencoded({ extended: true }))
app.use(express.json());

app.use('/auth', authRouter);
app.use('/articles', blogRouter);
app.use('/users', usersRouter);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    const addr = server.address();
    const bind = 'port ' + addr.port;
    console.log('Listening on ' + bind);
}
