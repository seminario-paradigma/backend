const router = require('express').Router();
const { check } = require('express-validator');

const { passwordEncoding, checkValidation, updateLastseen } = require('../../utils/validation')
const jwtUtils = require('../../utils/jwt')

const mongoose = require('mongoose')

const User = require('../../mongo/model/user');
const Article = require('../../mongo/model/article');

router.get('/me', jwtUtils.jwtIsValid, function (req, res) {
    user = res.locals.decodedUser;
    updateLastseen(user);
    return res.json(user);
})

router.put('/me', /*[
    check('email').isEmail(),
    check('username').isString().isLength({ min: 3, max: 32 }),
], checkValidation,*/ jwtUtils.jwtIsValid, function (req, res) {
    decodedUser = res.locals.decodedUser;

    user = {};
    user.lastseen = new Date(Date.now()).toISOString();

    if (req.body.username)
        user.username = req.body.username;
    if (req.body.email)
        user.email = req.body.email;
    if (req.body.firstname)
        user.firstname = req.body.firstname;
    if (req.body.lastname)
        user.lastname = req.body.lastname;
    if (req.body.gender)
        user.gender = req.body.gender;
    if (req.body.profile_image)
        user.profile_image = req.body.profile_image;
    if (req.body.birth_date)
        user.birth_date = req.body.birth_date;
    User.findOneAndUpdate({ _id: decodedUser._id }, user, {new: true}, function (err, result) {
        if (err) {
            if (err.kind === 'ObjectId')
                res.status(400).send("User id is malformed")
            else
                console.log(err);
        }
        else if (!result)
            res.status('404').send('No user with id ' + req.params.id + ' found');
        else
            res.json(jwtUtils.generateJWT(result));
            
    })
})

router.put('/me/password', [
    check('password').isString().isLength({ min: 8, max: 32 }),
], checkValidation, jwtUtils.jwtIsValid, function (req, res) {
    User.findOneAndUpdate({ _id: res.locals.decodedUser._id }, {"password": passwordEncoding(req.body.password) }, { new: true }, function (err, user) {
        if (err) {
            if (err.kind === 'ObjectId')
                res.status(400).send("User id is malformed")
            else
                console.log(err);
        }
        else if (!user)
            res.status('404').send('No user with id ' + req.params.id + ' found');
        else {
            user.password = undefined;
            res.json(user);
        }
    })
})

router.delete('/me', jwtUtils.jwtIsValid, function (req, res) {
    User.deleteOne({ _id: res.locals.decodedUser._id }, function (err) {
        if (err !== null) {
            if (err.kind === 'ObjectId')
                res.status(400).send("User id is malformed")
            else
                console.log(err);
        }
    })
    res.send();
})

router.get('/search', function (req, res) {
    const searchQuery = req.query.query;
    User.find({ $text: { $search: searchQuery, $caseSensitive: false } })
        .exec(function (err, users) {
            if (err)
                console.log(err);
            if (users.length === 0 || !users)
                res.status('404').send('No users ' + searchQuery + ' found');
            else {
                users.forEach(user => user.password = undefined);
                res.json(users);
            }
        })
})

router.get('/:id', function (req, res) {
    User.findOne({ _id: req.params.id }).exec(function (err, user) {
        if (err) {
            if (err.kind === 'ObjectId')
                res.status(400).send("User id is malformed")
            else
                console.log(err);
        }
        else if (!user)
            res.status('404').send('No user with id ' + req.params.id + ' found');
        else {
            user.password = undefined;
            res.json(user);
        }
    })
})

router.get('/:id/comments', jwtUtils.jwtIsValid, function (req, res) {
    Article.aggregate([
        {
            $project: {
                comments: {
                    $filter: {
                        input: "$comments",
                        as: "comment",
                        cond: { $eq: ["$$comment.from", mongoose.Types.ObjectId(req.params.id)] }
                    }
                }
            }
        },
        {
            $unwind: "$comments"
        }
    ])
        .exec(function (err, comments) {
            if (err)
                console.log(err);
            else if (!comments || comments.length === 0)
                res.status(404).send("User " + req.params.id + " has no comments yet");
            else
                res.json(comments);
        })
})

module.exports = router;