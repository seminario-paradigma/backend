const router = require('express').Router();
const { check } = require('express-validator');

const { passwordEncoding, checkValidation, updateLastseen } = require('../../utils/validation')
const jwtUtils = require('../../utils/jwt')


const Article = require('../../mongo/model/article');
const Comment = require('../../mongo/model/comment');
const { query } = require('express');

router.get('/', function (req, res) {
    Article.find({}).sort({ "created_at": "desc" }).exec(function (err, articles) {
        if (err)
            console.log(err)
        else
            res.json(articles);
    })
})

router.post('/new', jwtUtils.jwtIsValid, [
    check('content').isString().isLength({ min: 20 }),
    check('title').isString(),
    check('hashtags').matches(new RegExp("^#[\\w-]+(?:\\s+#[\\w-]+)*$"))
], checkValidation, function (req, res) {
    user = res.locals.decodedUser;
    updateLastseen(user);
    if (!user.is_admin)
        res.status("403").send("Only admins can create new articles");
    else {
        let adminId = user._id;
        let newArticle = new Article({ written_by: adminId, content: req.body.content, title: req.body.title, hashtags: req.body.hashtags });
        Article.create(newArticle, function (err, doc) {
            if (err) {
                if (err.code === 11000)
                    res.status(409).send("Duplicated");
                else
                    res.status(400).send("Unknown error");
            }
            else
                res.send(doc);
        });
    }
});

router.post('/:id/newcomment', jwtUtils.jwtIsValid, [check('text')], checkValidation,
    function (req, res) {
        user = res.locals.decodedUser;
        updateLastseen(user);
        Article.findOne({ _id: req.params.id }).exec(function (err, article) {
            if (err) {
                if (err.kind === 'ObjectId')
                    res.status(400).send("Article id is malformed")
                else
                    console.log(err);
            }
            else if (!article)
                res.status('404').send('Article with id ' + req.params.id + ' doesn\'t exists');
            else {
                const comment = new Comment({ from: user._id, text: req.body.text })
                article.comments.push(comment);
                article.save().then(function (doc, err) {
                    if (err) {
                        console.log(err);
                        res.status('500').send("");
                    }
                    else
                        res.json(doc);
                });
            }
        })
    });


router.get('/search', function (req, res) {
    const searchQuery = req.query.query;
    if (query)
        Article.find({ $text: { $search: searchQuery, $caseSensitive: false } })
            .exec(function (err, articles) {
                if (err)
                    console.log(err);
                if (articles.length === 0 || !articles)
                    res.status('404').send('No articles matches the specified query: ' + searchQuery);
                else
                    res.json(articles);
            })
    else
        res.status(400).send("no query specified");
})


router.get('/:id', function (req, res) {
    Article.findOne({ _id: req.params.id }).exec(function (err, article) {
        if (err)
            if (err.kind === 'ObjectId')
                res.status(400).send("Article id is malformed")
            else
                console.log(err);
        if (!article)
            res.status('404').send('Article with id ' + req.params.id + ' doesn\'t exists');
        else
            res.json(article);
    })
});


router.put('/:id', jwtUtils.jwtIsValid, [check('content').isString().isLength({ min: 20 }),
    check('title').isString(),
    check('hashtags').matches(new RegExp("^#[\\w-]+(?:\\s+#[\\w-]+)*$"))
    ], checkValidation, function (req, res) {
    user = res.locals.decodedUser;
    updateLastseen(user);
    if (!user.is_admin)
        res.status("403").send("Only admins can update articles");

    article = {};
    article.last_modified = new Date(Date.now()).toISOString();

    if (req.body.title)
        article.title = req.body.title;
    if (req.body.content)
        article.content = req.body.content;
    if (req.body.hashtags)
        article.hashtags = req.body.hashtags;
    Article.findOneAndUpdate({ _id: req.params.id }, article, { new: true }, function (err, modifiedArticle) {
        if (err) {
            if (err.kind === 'ObjectId')
                res.status(400).send("Article id is malformed")
            else
                console.log(err);
        }
        else if (!modifiedArticle)
            res.status('404').send('No article with id ' + req.params.id + ' found');
        else
            res.json(modifiedArticle);

    })
});

module.exports = router;