const router = require('express').Router();
const { check } = require('express-validator');

const { passwordEncoding, checkValidation, updateLastseen } = require('../../utils/validation')
const jwtUtils = require('../../utils/jwt')


const User = require('../../mongo/model/user');

router.post('/register', [
    check('email').isEmail(),
    check('username').isString().isLength({ min: 3, max: 32 }),
    check('password').isString().isLength({ min: 8, max: 64 })
], checkValidation, function (req, res) {
    let password = passwordEncoding(req.body.password);
    let newUser = new User({ email: req.body.email, username: req.body.username, password: password });
    User.create(newUser, function (err, doc) {
        if (err)
            if (err.keyPattern.email !== undefined)
                res.status(409).send("Email already exists");
            else
                res.status(400).send("Unknown error");
        else
            res.send(doc._id);
    });
})

router.post('/login', [
    check('email').isEmail(),
    check('password').isString().isLength({ min: 8 })
], checkValidation, function (req, res) {
    User.findOne({ email: req.body.email, password: passwordEncoding(req.body.password) }).exec(function (err, user) {
        if (err)
            console.log(err);
        if (user === null)
            res.status('404').send('Wrong email or password');
        else {
            updateLastseen(user);
            user.password = undefined;
            res.json(jwtUtils.generateJWT(user));
        }
    })
})

module.exports = router;