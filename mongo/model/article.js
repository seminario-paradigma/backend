const mongoose = require('mongoose');

const Comment = require("./comment").schema;

const articleSchema = new mongoose.Schema({
    written_by: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
    title: { type: String, required: true, unique: true },
    content: { type: String, required: true },
    created_at: { type: Date, default: Date.now() },
    last_modified: { type: Date, default: Date.now() },
    hashtags: { type: String },
    comments: [ Comment ]
});
articleSchema.index({ 'title': 'text', 'content': 'text', 'hashtags' : 'text' });

const Article = mongoose.model('Article', articleSchema);
module.exports = Article;