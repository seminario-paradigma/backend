const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, require: true, unique: true},
    username: { type: String, required: true },
    password: { type: String, required: true },
    lastseen: { type: Date, default: Date.now() },
    is_admin: { type: Boolean, default: false },
    firstname: { type: String, required: false },
    lastname: { type: String, required: false },
    gender: { type: String, required: false },
    profile_image: { type: String, required: false },
    birth_date: { type: Date, required: false}
});
userSchema.index({ name: 'text', 'username': 'text', 'email': 'text', 'firstname' : 'text', 'lastname' : 'text' });

let User = mongoose.model('User', userSchema);
module.exports = User;