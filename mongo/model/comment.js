const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
    from: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User', index: true },
    text: { type: String, required: true },
    created_at: { type: Date, default: Date.now() },
    last_modified: { type: Date, default: Date.now() }
});

const Comment = mongoose.model('Comment', commentSchema);
module.exports = Comment;